output "subnetwork" {
  description = "A reference (self_link) to the VPC network"
  value       = google_compute_subnetwork.vpc_subnetwork.self_link
}

output "subnetwork_name" {
  value = google_compute_subnetwork.vpc_subnetwork.name
}

output "subnetwork_id" {
  value = google_compute_subnetwork.vpc_subnetwork.id
}
