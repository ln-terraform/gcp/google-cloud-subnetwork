module "management_subnetwork" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "github.com/gruntwork-io/terraform-google-network.git//modules/vpc-network?ref=v0.1.2"
  source = "../"

  project                  = var.project
  network                  = "projects/gcloud-study-312204/global/networks/test-vpc-network"
  name                     = "test-vpc-subnetwork-asia-southeast1"
  region                   = "asia-southeast1"
  private_ip_google_access = true
  primary_ip_cidr_range    = "192.168.1.0/24"
  secondary_ip_cidr_ranges = [
    {
      range_name    = "test"
      ip_cidr_range = "10.1.0.0/20"
    }
  ]
  log_config = null
}
