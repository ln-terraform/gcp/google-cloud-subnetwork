output "subnetwork" {
  description = "A reference (self_link) to the VPC subnetwork"
  value       = module.management_subnetwork.subnetwork
}

output "subnetwork_name" {
  value = module.management_subnetwork.subnetwork_name
}

output "subnetwork_id" {
  value = module.management_subnetwork.subnetwork_id
}
